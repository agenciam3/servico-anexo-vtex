# servico-anexo-vtex
Um script para adicionar anexos a serviços da VTEX

##Requerimentos
Requer jQuery 1.8+ e vtex.js 1.0+

##Aviso Importante
No momento, o script está customizado apenas para a loja RKT Esportes.
O script requer alguns passos de configuração no painel administrativo.
De modo genérico, os passos são os seguintes:

1. Criação de um tipo de serviço
2. Criação de um anexo para o serviço
3. Criação de uma tabela de valores para o serviço
4. Vinculação de SKUs ao serviço criado

É necessário certificar-se de que o sistema da VTEX indexou corretamente os produtos e serviços antes de tentar programar qualquer coisa.

##Modo de Usar
Como está customizado para a RKT, é só adicionar o script em algum lugar do template específico para produtos customizados. Há uma rudimentar validação de formulário que adiciona a classe `rkt-attach-invalid` ao elemento input com erro, o que permite flexibilidade de customização.

##To-do
- Generalizar o script para transformar em um plugin e ser útil para mais casos
- Criar opções diversas para o plugin e uma checagem mais consistente de erros