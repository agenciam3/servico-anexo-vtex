/* 
Nome e número customizados em camiseta
Script para VTEX
Requer jQuery 1.8+
Requer vtex.js 1.0+
Autor: Pedro Duarte Freires 
Contatos: {
	'Github': 'https://github.com/freires/servico-anexo-vtex',
	'Linkedin' : 'https://br.linkedin.com/in/pedrofreires'
}
MIT License
*/
//Valida o nome
$('.customizable #nome').change(function(){
	var userInput = $(this).val();
	var re = /^\D{1,12}$/;
	if ((userInput != '') && !(re.test(userInput))) {
		$(this).addClass('rkt-attach-invalid');
	} else {
		$(this).removeClass('rkt-attach-invalid');
	}
});

//Valida o número
$('.customizable #numero').change(function(){
	var userInput = $(this).val();
	var re = /^\d{1,2}$/;
	if ((userInput != '') && !(re.test(userInput))) {
		$(this).addClass('rkt-attach-invalid');
	} else {
		$(this).removeClass('rkt-attach-invalid');
	}
});

//Mantém o "Carregando..." ativo enquanto não tiver terminado de incluir serviços
$(window).on('rkt-offer-update',function(tipo){
	var rktNome, rktNumero;
	rktNome = $('.customizable #nome').val();
	rktNumero = $('.customizable #numero').val();	
	if((rktNome && rktNumero) && (tipo == 'numero')) {
		$('#ajaxBusy').hide();
		$('.customizable #nome, .customizable #numero').prop('disabled', false);
	}
	else if(rktNome || rktNumero) {
		$('#ajaxBusy').hide();
		$('.customizable #nome, .customizable #numero').prop('disabled', false);
	}
});

//Processa os serviços
$(window).on('cartProductAdded.vtex',function(){
	if($('.customizable').length){
		var rktNome, rktNumero, rktNomeValido, rktNumeroValido, indexUltimo, bundleItemLastIndex, nomeOfferId, numeroOfferId;
		rktNome = $('.customizable #nome').val();
		rktNumero = $('.customizable #numero').val();
		rktNomeValido = (rktNome) && (!$('.customizable #nome').hasClass('rkt-attach-invalid'));
		rktNumeroValido = (rktNumero) && (!$('.customizable #numero').hasClass('rkt-attach-invalid'));
		if (rktNomeValido||rktNumeroValido){
			$('#ajaxBusy').show();
			$('.customizable #nome, .customizable #numero').prop('disabled', true);
			if (rktNome){
				vtexjs.checkout.getOrderForm()
				.then(function(orderForm){
					indexUltimo = orderForm.items.length - 1;
					nomeOfferId = orderForm.items[indexUltimo].offerings[0].id;
				    return vtexjs.checkout.addOffering(nomeOfferId, indexUltimo);
				})
				.then(function(orderForm){
					bundleItemLastIndex = orderForm.items[indexUltimo].bundleItems.length - 1;
					var bundleId = orderForm.items[indexUltimo].bundleItems[bundleItemLastIndex].id; 
					var content = {'Value' : rktNome};
					return vtexjs.checkout.addBundleItemAttachment(indexUltimo, bundleId, 'rktCustomValue', content);
				})
				.done(function(orderForm){
					$(window).trigger('rkt-offer-update',['nome']);
				    //Ações que devem ocorrer após inserir nome com sucesso
				});
			}
			if (rktNumero){
				vtexjs.checkout.getOrderForm()
				.then(function(orderForm){
					indexUltimo = orderForm.items.length - 1;
					nomeOfferId = orderForm.items[indexUltimo].offerings[1].id;
				    return vtexjs.checkout.addOffering(nomeOfferId, indexUltimo);
				})
				.then(function(orderForm){
					bundleItemLastIndex = orderForm.items[indexUltimo].bundleItems.length - 1;
					var bundleId = orderForm.items[indexUltimo].bundleItems[bundleItemLastIndex].id; 
					var content = {'Value' : rktNumero};
					return vtexjs.checkout.addBundleItemAttachment(indexUltimo, bundleId, 'rktCustomValue', content);
				})
				.done(function(orderForm){
					$(window).trigger('rkt-offer-update',['numero']);
				    //Ações que devem ocorrer após inserir número com sucesso
				});
			}
		} else {
			console.log("Nenhuma oferta foi preenchida");
		}
	}
});